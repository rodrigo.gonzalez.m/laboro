    define( ['require'], function( require ){
        function func( Apperyio ){
            var manager = {
                orderId: null,
                operatorId: null,
                operatorName: null,
                operatorUsername: null,
                stateList: null
            };
            return manager;
        }

        return [{
            /* name for angular resource */
            name: 'userManager',
            /* type of angular resource */
            type: 'factory',
            /* angular dependency injection array */
            deps: [ 'Apperyio', func ]
        }];
    });