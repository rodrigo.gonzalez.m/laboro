define(['require'], function(require) {
    /**
     * Models generated from "Model and Storage" and models extracted from services.
     * To generate entity use syntax:
     * Apperyio.EntityAPI("<model_name>[.<model_field>]");
     */
    var models = {
        "OperatorList": {
            "type": "array",
            "items": [{
                "type": "Operator"
            }]
        },
        "GpsRecentItem": {
            "type": "object",
            "properties": {
                "longitud": {
                    "type": "string"
                },
                "usuario": {
                    "type": "string"
                },
                "deviceid": {
                    "type": "string"
                },
                "latitud": {
                    "type": "string"
                },
                "fechaHora": {
                    "type": "string"
                },
                "_id": {
                    "type": "string"
                },
                "posHist": {
                    "type": "string"
                }
            }
        },
        "CompanyList": {
            "type": "array",
            "items": [{
                "type": "Company"
            }]
        },
        "File": {
            "type": "object",
            "properties": {
                "file": {
                    "type": "object",
                    "properties": {
                        "name": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "Strings": {
            "type": "array",
            "items": [{
                "type": "texto"
            }]
        },
        "FIles": {
            "type": "array",
            "items": [{
                "type": "object",
                "properties": {
                    "filepointer": {
                        "type": "object",
                        "properties": {}
                    },
                    "filename": {
                        "type": "string"
                    }
                }
            }]
        },
        "listaEstadosOperacion": {
            "type": "array",
            "items": [{
                "type": "estadoOperacion"
            }]
        },
        "UserList": {
            "type": "array",
            "items": [{
                "type": "User"
            }]
        },
        "Ordenes": {
            "type": "array",
            "items": [{
                "type": "Orden"
            }]
        },
        "objeto": {
            "type": "object",
            "properties": {}
        },
        "String": {
            "type": "string"
        },
        "Orden": {
            "type": "object",
            "properties": {
                "diagEspecialistaTexto": {
                    "type": "string"
                },
                "codigo": {
                    "type": "string"
                },
                "checkList": {
                    "type": "string"
                },
                "cliente": {
                    "type": "string"
                },
                "comentarioCierre": {
                    "type": "string"
                },
                "fechaCompra": {
                    "type": "string"
                },
                "tipoServicio": {
                    "type": "string"
                },
                "_id": {
                    "type": "string"
                },
                "marca": {
                    "type": "string"
                },
                "fechaInicioServicio": {
                    "type": "string"
                },
                "posGpsFinServicio": {
                    "type": "string"
                },
                "ingeniero": {
                    "type": "string"
                },
                "articulo": {
                    "type": "string"
                },
                "fechaEstimada": {
                    "type": "string"
                },
                "fechaSolicitud": {
                    "type": "string"
                },
                "correoCliente": {
                    "type": "string"
                },
                "descripcionDefecto": {
                    "type": "string"
                },
                "trabEspecialistaTexto": {
                    "type": "string"
                },
                "investigacion": {
                    "type": "string"
                },
                "telefonoCliente": {
                    "type": "string"
                },
                "fechaEjecucion": {
                    "type": "string"
                },
                "mensajeCliente": {
                    "type": "string"
                },
                "descripcionReparacion": {
                    "type": "string"
                },
                "direccion": {
                    "type": "string"
                },
                "comuna": {
                    "type": "string"
                },
                "estado": {
                    "type": "string"
                },
                "repuestos": {
                    "type": "string"
                }
            }
        },
        "Boolean": {
            "type": "boolean"
        },
        "Photo": {
            "type": "object",
            "properties": {
                "imagen": {
                    "type": "string"
                }
            }
        },
        "Markers": {
            "type": "array",
            "items": [{
                "type": "string"
            }]
        },
        "Photos": {
            "type": "array",
            "items": [{
                "type": "Photo"
            }]
        },
        "Company": {
            "type": "object",
            "properties": {
                "nombreCliente": {
                    "type": "string"
                },
                "clienteLogo": {
                    "type": "string"
                },
                "dirCliente": {
                    "type": "string"
                },
                "telefonoCliente": {
                    "type": "string"
                },
                "emailCliente": {
                    "type": "string"
                },
                "idCliente": {
                    "type": "string"
                }
            }
        },
        "Number": {
            "type": "number"
        },
        "NamesList": {
            "type": "array",
            "items": [{
                "type": "Names"
            }]
        },
        "User": {
            "type": "object",
            "properties": {
                "codigoOperador": {
                    "type": "string"
                },
                "email": {
                    "type": "string"
                },
                "fullName": {
                    "type": "string"
                },
                "username": {
                    "type": "string"
                },
                "token": {
                    "type": "string"
                },
                "_id": {
                    "type": "string"
                }
            }
        },
        "texto": {
            "type": "object",
            "properties": {
                "texto1": {
                    "type": "string"
                }
            }
        },
        "Operator": {
            "type": "object",
            "properties": {
                "idCliente": {
                    "type": "string"
                },
                "estadoOperador": {
                    "type": "string"
                },
                "idOperador": {
                    "type": "string"
                },
                "profile": {
                    "type": "string"
                },
                "lastReportGps": {
                    "type": "string"
                }
            }
        },
        "GpsRecentList": {
            "type": "array",
            "items": [{
                "type": "GpsRecentItem"
            }]
        },
        "MultimediaData": {
            "type": "array",
            "items": [{
                "type": "string"
            }]
        },
        "Names": {
            "type": "object",
            "properties": {
                "codigoOperador": {
                    "type": "string"
                },
                "fullName": {
                    "type": "string"
                }
            }
        },
        "estadoOperacion": {
            "type": "object",
            "properties": {
                "fecha": {
                    "type": "string"
                },
                "estado": {
                    "type": "boolean"
                }
            }
        },
        "Tech1_OPERATIONDAY_create_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/OPERATIONDAY"
                },
                "method": {
                    "type": "string",
                    "default": "post"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "idEmpresa": {
                                    "type": "string"
                                },
                                "fechaOperacion": {
                                    "type": "string"
                                },
                                "acl": {
                                    "type": "object",
                                    "properties": {
                                        "*": {
                                            "type": "object",
                                            "properties": {
                                                "read": {
                                                    "type": "boolean",
                                                    "default": true
                                                },
                                                "write": {
                                                    "type": "boolean",
                                                    "default": true
                                                }
                                            }
                                        }
                                    }
                                },
                                "estadoOperacion": {
                                    "type": "string"
                                }
                            }
                        },
                        "query": {
                            "type": "object",
                            "properties": {}
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                },
                                "Content-Type": {
                                    "type": "string",
                                    "default": "application/json"
                                },
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "_id": {
                                    "type": "string"
                                },
                                "_createdAt": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_signup_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/users"
                },
                "method": {
                    "type": "string",
                    "default": "post"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "username": {
                                    "type": "string"
                                },
                                "password": {
                                    "type": "string"
                                }
                            }
                        },
                        "query": {
                            "type": "object",
                            "properties": {}
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                },
                                "Content-Type": {
                                    "type": "string",
                                    "default": "application/json"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "_id": {
                                    "type": "string"
                                },
                                "_createdAt": {
                                    "type": "string"
                                },
                                "_updatedAt": {
                                    "type": "string"
                                },
                                "username": {
                                    "type": "string"
                                },
                                "sessionToken": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_me_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/users/me"
                },
                "method": {
                    "type": "string",
                    "default": "get"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {}
                        },
                        "query": {
                            "type": "object",
                            "properties": {}
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                },
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "object",
                                    "properties": {
                                        "username": {
                                            "type": "string"
                                        },
                                        "_createdAt": {
                                            "type": "string"
                                        },
                                        "email": {
                                            "type": "string"
                                        },
                                        "_id": {
                                            "type": "string"
                                        },
                                        "acl": {
                                            "type": "object",
                                            "properties": {
                                                "*": {
                                                    "type": "object",
                                                    "properties": {
                                                        "read": {
                                                            "type": "boolean",
                                                            "default": true
                                                        },
                                                        "write": {
                                                            "type": "boolean",
                                                            "default": true
                                                        }
                                                    }
                                                }
                                            }
                                        },
                                        "_updatedAt": {
                                            "type": "string"
                                        },
                                        "fullName": {
                                            "type": "string"
                                        }
                                    }
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_GPS_query_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/GPS"
                },
                "method": {
                    "type": "string",
                    "default": "get"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {}
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "where": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                },
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "array",
                                    "items": [{
                                        "type": "object",
                                        "properties": {
                                            "_updatedAt": {
                                                "type": "string"
                                            },
                                            "idCliente": {
                                                "type": "string"
                                            },
                                            "fechaReporte": {
                                                "type": "string"
                                            },
                                            "longitud": {
                                                "type": "string"
                                            },
                                            "latitud": {
                                                "type": "string"
                                            },
                                            "deviceid": {
                                                "type": "string"
                                            },
                                            "acl": {
                                                "type": "object",
                                                "properties": {
                                                    "*": {
                                                        "type": "object",
                                                        "properties": {
                                                            "read": {
                                                                "type": "boolean",
                                                                "default": true
                                                            },
                                                            "write": {
                                                                "type": "boolean",
                                                                "default": true
                                                            }
                                                        }
                                                    }
                                                }
                                            },
                                            "_id": {
                                                "type": "string"
                                            },
                                            "usuario": {
                                                "type": "string"
                                            },
                                            "_createdAt": {
                                                "type": "string"
                                            },
                                            "posHist": {
                                                "type": "string"
                                            }
                                        }
                                    }]
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_ORDERS_delete_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/ORDERS/{_id}"
                },
                "method": {
                    "type": "string",
                    "default": "delete"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {}
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "_id": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                },
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "object",
                                    "properties": {}
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_ORDERS_create_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/ORDERS"
                },
                "method": {
                    "type": "string",
                    "default": "post"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "trabEspecialistaAudio": {
                                    "type": "string"
                                },
                                "fechaSolicitud": {
                                    "type": "string"
                                },
                                "fechaEjecucion": {
                                    "type": "string"
                                },
                                "fotos": {
                                    "type": "string"
                                },
                                "subTipoServicio": {
                                    "type": "string"
                                },
                                "articulo": {
                                    "type": "string"
                                },
                                "tipoServicio": {
                                    "type": "string"
                                },
                                "enDispositivo": {
                                    "type": "boolean",
                                    "default": null
                                },
                                "idEmpresa": {
                                    "type": "string"
                                },
                                "correoCliente": {
                                    "type": "string"
                                },
                                "diagEspecialistaTexto": {
                                    "type": "string"
                                },
                                "acl": {
                                    "type": "object",
                                    "properties": {
                                        "*": {
                                            "type": "object",
                                            "properties": {
                                                "read": {
                                                    "type": "boolean",
                                                    "default": true
                                                },
                                                "write": {
                                                    "type": "boolean",
                                                    "default": true
                                                }
                                            }
                                        }
                                    }
                                },
                                "telefonoCliente": {
                                    "type": "string"
                                },
                                "descripcionReparacion": {
                                    "type": "string"
                                },
                                "investigacion": {
                                    "type": "string"
                                },
                                "trabEspecialistaTexto": {
                                    "type": "string"
                                },
                                "diagEspecialistaAudio": {
                                    "type": "string"
                                },
                                "fechaAsignacion": {
                                    "type": "string"
                                },
                                "comentarioCierre": {
                                    "type": "string"
                                },
                                "estado": {
                                    "type": "number",
                                    "default": null
                                },
                                "nombreCliente": {
                                    "type": "string"
                                },
                                "idOperador": {
                                    "type": "string"
                                },
                                "mensajeCliente": {
                                    "type": "string"
                                },
                                "checkList": {
                                    "type": "string"
                                },
                                "direccion": {
                                    "type": "string"
                                },
                                "codigo": {
                                    "type": "string"
                                },
                                "comuna": {
                                    "type": "string"
                                },
                                "posGpsFinServicio": {
                                    "type": "string"
                                },
                                "posGpsFinalizar": {
                                    "type": "string"
                                },
                                "tipoCancelacion": {
                                    "type": "string"
                                },
                                "fechaInicioServicio": {
                                    "type": "string"
                                },
                                "marca": {
                                    "type": "string"
                                },
                                "descripcionDefecto": {
                                    "type": "string"
                                },
                                "repuestos": {
                                    "type": "string"
                                },
                                "fechaEstimada": {
                                    "type": "string"
                                },
                                "fechaCompra": {
                                    "type": "string"
                                }
                            }
                        },
                        "query": {
                            "type": "object",
                            "properties": {}
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                },
                                "Content-Type": {
                                    "type": "string",
                                    "default": "application/json"
                                },
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "_createdAt": {
                                    "type": "string"
                                },
                                "_id": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_GPS_read_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/GPS/{_id}"
                },
                "method": {
                    "type": "string",
                    "default": "get"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {}
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "_id": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                },
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "object",
                                    "properties": {
                                        "idCliente": {
                                            "type": "string"
                                        },
                                        "_createdAt": {
                                            "type": "string"
                                        },
                                        "posHist": {
                                            "type": "string"
                                        },
                                        "fechaReporte": {
                                            "type": "string"
                                        },
                                        "usuario": {
                                            "type": "string"
                                        },
                                        "_id": {
                                            "type": "string"
                                        },
                                        "deviceid": {
                                            "type": "string"
                                        },
                                        "longitud": {
                                            "type": "string"
                                        },
                                        "_updatedAt": {
                                            "type": "string"
                                        },
                                        "latitud": {
                                            "type": "string"
                                        },
                                        "acl": {
                                            "type": "object",
                                            "properties": {
                                                "*": {
                                                    "type": "object",
                                                    "properties": {
                                                        "write": {
                                                            "type": "boolean",
                                                            "default": true
                                                        },
                                                        "read": {
                                                            "type": "boolean",
                                                            "default": true
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_OPERATIONDAY_query_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/OPERATIONDAY"
                },
                "method": {
                    "type": "string",
                    "default": "get"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {}
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "where": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                },
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "array",
                                    "items": [{
                                        "type": "object",
                                        "properties": {
                                            "idEmpresa": {
                                                "type": "string"
                                            },
                                            "_updatedAt": {
                                                "type": "string"
                                            },
                                            "_id": {
                                                "type": "string"
                                            },
                                            "_createdAt": {
                                                "type": "string"
                                            },
                                            "acl": {
                                                "type": "object",
                                                "properties": {
                                                    "*": {
                                                        "type": "object",
                                                        "properties": {
                                                            "read": {
                                                                "type": "boolean",
                                                                "default": true
                                                            },
                                                            "write": {
                                                                "type": "boolean",
                                                                "default": true
                                                            }
                                                        }
                                                    }
                                                }
                                            },
                                            "fechaOperacion": {
                                                "type": "string"
                                            },
                                            "estadoOperacion": {
                                                "type": "string"
                                            }
                                        }
                                    }]
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "GetGeoLocationFromAddress_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/code/7dfaef8b-b6ff-4187-9fb7-453ea958793c/exec"
                },
                "method": {
                    "type": "string",
                    "default": "get"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {}
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "sensor": {
                                    "type": "string"
                                },
                                "address": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "object",
                                    "properties": {
                                        "requestParams": {
                                            "type": "string"
                                        },
                                        "requestBody": {
                                            "type": "string"
                                        }
                                    }
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_user_update_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/users/{_id}"
                },
                "method": {
                    "type": "string",
                    "default": "put"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "username": {
                                    "type": "string"
                                },
                                "codigoOperador": {
                                    "type": "string"
                                },
                                "fullName": {
                                    "type": "string"
                                },
                                "acl": {
                                    "type": "object",
                                    "properties": {
                                        "*": {
                                            "type": "object",
                                            "properties": {
                                                "read": {
                                                    "type": "boolean",
                                                    "default": true
                                                },
                                                "write": {
                                                    "type": "boolean",
                                                    "default": true
                                                }
                                            }
                                        }
                                    }
                                },
                                "email": {
                                    "type": "string"
                                },
                                "password": {
                                    "type": "string"
                                },
                                "foto": {
                                    "type": "string"
                                }
                            }
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "_id": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                },
                                "Content-Type": {
                                    "type": "string",
                                    "default": "application/json"
                                },
                                "X-Appery-Master-Key": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id_su}"
                                },
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "_updatedAt": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_login_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/login"
                },
                "method": {
                    "type": "string",
                    "default": "post"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "username": {
                                    "type": "string"
                                },
                                "password": {
                                    "type": "string"
                                }
                            }
                        },
                        "query": {
                            "type": "object",
                            "properties": {}
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "Content-Type": {
                                    "type": "string",
                                    "default": "application/json"
                                },
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "sessionToken": {
                                    "type": "string"
                                },
                                "_id": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "updateMassiveOrdersState_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/code/1c96d2ab-2f69-4284-a5df-61b1dd648f33/exec"
                },
                "method": {
                    "type": "string",
                    "default": "post"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "data": {
                                    "type": "data"
                                }
                            }
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "listaCodigos": {
                                    "type": "string",
                                    "default": "[\"5c866b6dfca79a58717e326e\"]"
                                },
                                "fecha": {
                                    "type": "string",
                                    "default": "2019-12-12"
                                },
                                "idEmpresa": {
                                    "type": "string",
                                    "default": "SGAS"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "Content-Type": {
                                    "type": "string",
                                    "default": "text/plain"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "object",
                                    "properties": {
                                        "requestBody": {
                                            "type": "string"
                                        },
                                        "requestParams": {
                                            "type": "string"
                                        }
                                    }
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_ORDERS_batch_update_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/ORDERS"
                },
                "method": {
                    "type": "string",
                    "default": "put"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "update": {
                                    "type": "object",
                                    "properties": {
                                        "repuestos": {
                                            "type": "string"
                                        },
                                        "acl": {
                                            "type": "object",
                                            "properties": {
                                                "*": {
                                                    "type": "object",
                                                    "properties": {
                                                        "write": {
                                                            "type": "boolean",
                                                            "default": true
                                                        },
                                                        "read": {
                                                            "type": "boolean",
                                                            "default": true
                                                        }
                                                    }
                                                }
                                            }
                                        },
                                        "descripcionReparacion": {
                                            "type": "string"
                                        },
                                        "fechaInicioServicio": {
                                            "type": "string"
                                        },
                                        "posGpsFinServicio": {
                                            "type": "string"
                                        },
                                        "articulo": {
                                            "type": "string"
                                        },
                                        "idEmpresa": {
                                            "type": "string"
                                        },
                                        "estado": {
                                            "type": "number",
                                            "default": null
                                        },
                                        "fechaSolicitud": {
                                            "type": "string"
                                        },
                                        "posGpsFinalizar": {
                                            "type": "string"
                                        },
                                        "marca": {
                                            "type": "string"
                                        },
                                        "trabEspecialistaAudio": {
                                            "type": "string"
                                        },
                                        "tipoServicio": {
                                            "type": "string"
                                        },
                                        "descripcionDefecto": {
                                            "type": "string"
                                        },
                                        "comentarioCierre": {
                                            "type": "string"
                                        },
                                        "fechaEstimada": {
                                            "type": "string"
                                        },
                                        "tipoCancelacion": {
                                            "type": "string"
                                        },
                                        "fechaCompra": {
                                            "type": "string"
                                        },
                                        "subTipoServicio": {
                                            "type": "string"
                                        },
                                        "correoCliente": {
                                            "type": "string"
                                        },
                                        "trabEspecialistaTexto": {
                                            "type": "string"
                                        },
                                        "idOperador": {
                                            "type": "string"
                                        },
                                        "fechaEjecucion": {
                                            "type": "string"
                                        },
                                        "comuna": {
                                            "type": "string"
                                        },
                                        "fotos": {
                                            "type": "string"
                                        },
                                        "diagEspecialistaAudio": {
                                            "type": "string"
                                        },
                                        "enDispositivo": {
                                            "type": "boolean",
                                            "default": null
                                        },
                                        "investigacion": {
                                            "type": "string"
                                        },
                                        "mensajeCliente": {
                                            "type": "string"
                                        },
                                        "codigo": {
                                            "type": "string"
                                        },
                                        "diagEspecialistaTexto": {
                                            "type": "string"
                                        },
                                        "checkList": {
                                            "type": "string"
                                        },
                                        "direccion": {
                                            "type": "string"
                                        },
                                        "nombreCliente": {
                                            "type": "string"
                                        },
                                        "telefonoCliente": {
                                            "type": "string"
                                        },
                                        "fechaAsignacion": {
                                            "type": "string"
                                        }
                                    }
                                },
                                "where": {
                                    "type": "object",
                                    "properties": {}
                                }
                            }
                        },
                        "query": {
                            "type": "object",
                            "properties": {}
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                },
                                "Content-Type": {
                                    "type": "string",
                                    "default": "application/json"
                                },
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "_updatedAt": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_WORKORDER_batch_update_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/WORKORDER"
                },
                "method": {
                    "type": "string",
                    "default": "put"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "update": {
                                    "type": "object",
                                    "properties": {
                                        "requestDate": {
                                            "type": "string"
                                        },
                                        "initOperatorId": {
                                            "type": "string"
                                        },
                                        "receiverContact": {
                                            "type": "object",
                                            "properties": {}
                                        },
                                        "formSubType": {
                                            "type": "string"
                                        },
                                        "initDate": {
                                            "type": "string"
                                        },
                                        "updatedBy": {
                                            "type": "string"
                                        },
                                        "previousDiagnosis": {
                                            "type": "object",
                                            "properties": {}
                                        },
                                        "productAntecedent": {
                                            "type": "object",
                                            "properties": {}
                                        },
                                        "operatorCode": {
                                            "type": "string"
                                        },
                                        "assignmentDate": {
                                            "type": "string"
                                        },
                                        "serviceAntecedent": {
                                            "type": "object",
                                            "properties": {}
                                        },
                                        "documentCode": {
                                            "type": "string"
                                        },
                                        "endOperatorId": {
                                            "type": "string"
                                        },
                                        "preferredDate": {
                                            "type": "string"
                                        },
                                        "formType": {
                                            "type": "string"
                                        },
                                        "companyId": {
                                            "type": "string"
                                        },
                                        "rangeAttention": {
                                            "type": "number",
                                            "default": null
                                        },
                                        "operationData": {
                                            "type": "object",
                                            "properties": {}
                                        },
                                        "createdBy": {
                                            "type": "string"
                                        },
                                        "documentStatus": {
                                            "type": "number",
                                            "default": null
                                        },
                                        "maximumDate": {
                                            "type": "string"
                                        },
                                        "endLocation": {
                                            "type": "object",
                                            "properties": {}
                                        },
                                        "endDate": {
                                            "type": "string"
                                        },
                                        "receiverAntecedent": {
                                            "type": "object",
                                            "properties": {}
                                        },
                                        "assignmentLocation": {
                                            "type": "object",
                                            "properties": {}
                                        },
                                        "acl": {
                                            "type": "object",
                                            "properties": {
                                                "*": {
                                                    "type": "object",
                                                    "properties": {
                                                        "write": {
                                                            "type": "boolean",
                                                            "default": true
                                                        },
                                                        "read": {
                                                            "type": "boolean",
                                                            "default": true
                                                        }
                                                    }
                                                }
                                            }
                                        },
                                        "creatorId": {
                                            "type": "string"
                                        },
                                        "deviceData": {
                                            "type": "object",
                                            "properties": {}
                                        },
                                        "initLocation": {
                                            "type": "object",
                                            "properties": {}
                                        }
                                    }
                                },
                                "where": {
                                    "type": "object",
                                    "properties": {}
                                }
                            }
                        },
                        "query": {
                            "type": "object",
                            "properties": {}
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                },
                                "Content-Type": {
                                    "type": "string",
                                    "default": "application/json"
                                },
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "_updatedAt": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_PICTURES_query_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/PICTURES"
                },
                "method": {
                    "type": "string",
                    "default": "get"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {}
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "where": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                },
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "array",
                                    "items": [{
                                        "type": "object",
                                        "properties": {
                                            "idOrden": {
                                                "type": "string"
                                            },
                                            "_createdAt": {
                                                "type": "string"
                                            },
                                            "acl": {
                                                "type": "object",
                                                "properties": {
                                                    "*": {
                                                        "type": "object",
                                                        "properties": {
                                                            "write": {
                                                                "type": "boolean",
                                                                "default": true
                                                            },
                                                            "read": {
                                                                "type": "boolean",
                                                                "default": true
                                                            }
                                                        }
                                                    }
                                                }
                                            },
                                            "ordenImagen": {
                                                "type": "number",
                                                "default": null
                                            },
                                            "_id": {
                                                "type": "string"
                                            },
                                            "_updatedAt": {
                                                "type": "string"
                                            },
                                            "imagen": {
                                                "type": "string"
                                            }
                                        }
                                    }]
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_OPERATORS_update_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/OPERATORS/{_id}"
                },
                "method": {
                    "type": "string",
                    "default": "put"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "profile": {
                                    "type": "string"
                                },
                                "acl": {
                                    "type": "object",
                                    "properties": {
                                        "*": {
                                            "type": "object",
                                            "properties": {
                                                "write": {
                                                    "type": "boolean",
                                                    "default": true
                                                },
                                                "read": {
                                                    "type": "boolean",
                                                    "default": true
                                                }
                                            }
                                        }
                                    }
                                },
                                "lastSessionToken": {
                                    "type": "string"
                                },
                                "lastReportGps": {
                                    "type": "string"
                                },
                                "estadoOperador": {
                                    "type": "boolean",
                                    "default": null
                                },
                                "idOperador": {
                                    "type": "string"
                                },
                                "idCliente": {
                                    "type": "string"
                                },
                                "lastSessionHour": {
                                    "type": "string"
                                }
                            }
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "_id": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                },
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                },
                                "Content-Type": {
                                    "type": "string",
                                    "default": "application/json"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "_updatedAt": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_WORKORDER_create_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/WORKORDER"
                },
                "method": {
                    "type": "string",
                    "default": "post"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "initOperatorId": {
                                    "type": "string"
                                },
                                "assignmentDate": {
                                    "type": "string"
                                },
                                "maximumDate": {
                                    "type": "string"
                                },
                                "endLocation": {
                                    "type": "object",
                                    "properties": {}
                                },
                                "assignmentLocation": {
                                    "type": "object",
                                    "properties": {}
                                },
                                "requestDate": {
                                    "type": "string"
                                },
                                "documentCode": {
                                    "type": "string"
                                },
                                "formType": {
                                    "type": "string"
                                },
                                "formSubType": {
                                    "type": "string"
                                },
                                "endDate": {
                                    "type": "string"
                                },
                                "createdBy": {
                                    "type": "string"
                                },
                                "operationData": {
                                    "type": "object",
                                    "properties": {}
                                },
                                "serviceAntecedent": {
                                    "type": "object",
                                    "properties": {}
                                },
                                "previousDiagnosis": {
                                    "type": "object",
                                    "properties": {}
                                },
                                "rangeAttention": {
                                    "type": "number",
                                    "default": null
                                },
                                "receiverAntecedent": {
                                    "type": "object",
                                    "properties": {}
                                },
                                "receiverContact": {
                                    "type": "object",
                                    "properties": {}
                                },
                                "operatorCode": {
                                    "type": "string"
                                },
                                "acl": {
                                    "type": "object",
                                    "properties": {
                                        "*": {
                                            "type": "object",
                                            "properties": {
                                                "write": {
                                                    "type": "boolean",
                                                    "default": true
                                                },
                                                "read": {
                                                    "type": "boolean",
                                                    "default": true
                                                }
                                            }
                                        }
                                    }
                                },
                                "preferredDate": {
                                    "type": "string"
                                },
                                "companyId": {
                                    "type": "string"
                                },
                                "initLocation": {
                                    "type": "object",
                                    "properties": {}
                                },
                                "documentStatus": {
                                    "type": "number",
                                    "default": null
                                },
                                "creatorId": {
                                    "type": "string"
                                },
                                "deviceData": {
                                    "type": "object",
                                    "properties": {}
                                },
                                "initDate": {
                                    "type": "string"
                                },
                                "productAntecedent": {
                                    "type": "object",
                                    "properties": {}
                                },
                                "endOperatorId": {
                                    "type": "string"
                                },
                                "updatedBy": {
                                    "type": "string"
                                }
                            }
                        },
                        "query": {
                            "type": "object",
                            "properties": {}
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                },
                                "Content-Type": {
                                    "type": "string",
                                    "default": "application/json"
                                },
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "_id": {
                                    "type": "string"
                                },
                                "_createdAt": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_WORKORDERMULTIMEDIA_query_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/WORKORDERMULTIMEDIA"
                },
                "method": {
                    "type": "string",
                    "default": "get"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {}
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "where": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                },
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "array",
                                    "items": [{
                                        "type": "object",
                                        "properties": {
                                            "_updatedAt": {
                                                "type": "string"
                                            },
                                            "_id": {
                                                "type": "string"
                                            },
                                            "data": {
                                                "type": "string"
                                            },
                                            "_createdAt": {
                                                "type": "string"
                                            },
                                            "acl": {
                                                "type": "object",
                                                "properties": {
                                                    "*": {
                                                        "type": "object",
                                                        "properties": {
                                                            "write": {
                                                                "type": "boolean",
                                                                "default": true
                                                            },
                                                            "read": {
                                                                "type": "boolean",
                                                                "default": true
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }]
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_CLIENT_update_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/CLIENT/{_id}"
                },
                "method": {
                    "type": "string",
                    "default": "put"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "telefonoCliente": {
                                    "type": "string"
                                },
                                "idCliente": {
                                    "type": "string"
                                },
                                "dirCliente": {
                                    "type": "string"
                                },
                                "dataAdd": {
                                    "type": "object",
                                    "properties": {}
                                },
                                "geoPos": {
                                    "type": "string"
                                },
                                "acl": {
                                    "type": "object",
                                    "properties": {
                                        "*": {
                                            "type": "object",
                                            "properties": {
                                                "read": {
                                                    "type": "boolean",
                                                    "default": true
                                                },
                                                "write": {
                                                    "type": "boolean",
                                                    "default": true
                                                }
                                            }
                                        }
                                    }
                                },
                                "nombreCliente": {
                                    "type": "string"
                                },
                                "emailNotificaciones": {
                                    "type": "string"
                                },
                                "emailCliente": {
                                    "type": "string"
                                },
                                "clienteLogo": {
                                    "type": "string"
                                }
                            }
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "_id": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                },
                                "Content-Type": {
                                    "type": "string",
                                    "default": "application/json"
                                },
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "_updatedAt": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_WORKORDER_update_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/WORKORDER/{_id}"
                },
                "method": {
                    "type": "string",
                    "default": "put"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "previousDiagnosis": {
                                    "type": "object",
                                    "properties": {}
                                },
                                "companyId": {
                                    "type": "string"
                                },
                                "operatorCode": {
                                    "type": "string"
                                },
                                "createdBy": {
                                    "type": "string"
                                },
                                "operationData": {
                                    "type": "object",
                                    "properties": {}
                                },
                                "documentCode": {
                                    "type": "string"
                                },
                                "creatorId": {
                                    "type": "string"
                                },
                                "assignmentDate": {
                                    "type": "string"
                                },
                                "acl": {
                                    "type": "object",
                                    "properties": {
                                        "*": {
                                            "type": "object",
                                            "properties": {
                                                "read": {
                                                    "type": "boolean",
                                                    "default": true
                                                },
                                                "write": {
                                                    "type": "boolean",
                                                    "default": true
                                                }
                                            }
                                        }
                                    }
                                },
                                "initDate": {
                                    "type": "string"
                                },
                                "rangeAttention": {
                                    "type": "number",
                                    "default": null
                                },
                                "requestDate": {
                                    "type": "string"
                                },
                                "initLocation": {
                                    "type": "object",
                                    "properties": {}
                                },
                                "endOperatorId": {
                                    "type": "string"
                                },
                                "updatedBy": {
                                    "type": "string"
                                },
                                "maximumDate": {
                                    "type": "string"
                                },
                                "receiverContact": {
                                    "type": "object",
                                    "properties": {}
                                },
                                "initOperatorId": {
                                    "type": "string"
                                },
                                "preferredDate": {
                                    "type": "string"
                                },
                                "receiverAntecedent": {
                                    "type": "object",
                                    "properties": {}
                                },
                                "productAntecedent": {
                                    "type": "object",
                                    "properties": {}
                                },
                                "formSubType": {
                                    "type": "string"
                                },
                                "endLocation": {
                                    "type": "object",
                                    "properties": {}
                                },
                                "documentStatus": {
                                    "type": "number",
                                    "default": null
                                },
                                "endDate": {
                                    "type": "string"
                                },
                                "formType": {
                                    "type": "string"
                                },
                                "serviceAntecedent": {
                                    "type": "object",
                                    "properties": {}
                                },
                                "assignmentLocation": {
                                    "type": "object",
                                    "properties": {}
                                },
                                "deviceData": {
                                    "type": "object",
                                    "properties": {}
                                }
                            }
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "_id": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "Content-Type": {
                                    "type": "string",
                                    "default": "application/json"
                                },
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                },
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "_updatedAt": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "passwordRestore_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/code/8099c462-b15f-4978-b4a7-e36a7d35a785/exec"
                },
                "method": {
                    "type": "string",
                    "default": "post"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "data": {
                                    "type": "data"
                                }
                            }
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "username": {
                                    "type": "string"
                                },
                                "token": {
                                    "type": "string"
                                },
                                "password": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "Content-Type": {
                                    "type": "string",
                                    "default": "text/plain"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "object",
                                    "properties": {
                                        "requestBody": {
                                            "type": "string"
                                        },
                                        "requestParams": {
                                            "type": "string"
                                        }
                                    }
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_WORKORDER_read_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/WORKORDER/{_id}"
                },
                "method": {
                    "type": "string",
                    "default": "get"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {}
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "_id": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                },
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "object",
                                    "properties": {
                                        "preferredDate": {
                                            "type": "string"
                                        },
                                        "assignmentDate": {
                                            "type": "string"
                                        },
                                        "productAntecedent": {
                                            "type": "object",
                                            "properties": {}
                                        },
                                        "_id": {
                                            "type": "string"
                                        },
                                        "creatorId": {
                                            "type": "string"
                                        },
                                        "previousDiagnosis": {
                                            "type": "object",
                                            "properties": {}
                                        },
                                        "deviceData": {
                                            "type": "object",
                                            "properties": {}
                                        },
                                        "createdBy": {
                                            "type": "string"
                                        },
                                        "rangeAttention": {
                                            "type": "number",
                                            "default": null
                                        },
                                        "receiverAntecedent": {
                                            "type": "object",
                                            "properties": {}
                                        },
                                        "serviceAntecedent": {
                                            "type": "object",
                                            "properties": {}
                                        },
                                        "operationData": {
                                            "type": "object",
                                            "properties": {}
                                        },
                                        "initLocation": {
                                            "type": "object",
                                            "properties": {}
                                        },
                                        "documentStatus": {
                                            "type": "number",
                                            "default": null
                                        },
                                        "endDate": {
                                            "type": "string"
                                        },
                                        "documentCode": {
                                            "type": "string"
                                        },
                                        "_createdAt": {
                                            "type": "string"
                                        },
                                        "formType": {
                                            "type": "string"
                                        },
                                        "endOperatorId": {
                                            "type": "string"
                                        },
                                        "requestDate": {
                                            "type": "string"
                                        },
                                        "initOperatorId": {
                                            "type": "string"
                                        },
                                        "companyId": {
                                            "type": "string"
                                        },
                                        "maximumDate": {
                                            "type": "string"
                                        },
                                        "receiverContact": {
                                            "type": "object",
                                            "properties": {}
                                        },
                                        "_updatedAt": {
                                            "type": "string"
                                        },
                                        "updatedBy": {
                                            "type": "string"
                                        },
                                        "operatorCode": {
                                            "type": "string"
                                        },
                                        "acl": {
                                            "type": "object",
                                            "properties": {
                                                "*": {
                                                    "type": "object",
                                                    "properties": {
                                                        "write": {
                                                            "type": "boolean",
                                                            "default": true
                                                        },
                                                        "read": {
                                                            "type": "boolean",
                                                            "default": true
                                                        }
                                                    }
                                                }
                                            }
                                        },
                                        "endLocation": {
                                            "type": "object",
                                            "properties": {}
                                        },
                                        "initDate": {
                                            "type": "string"
                                        },
                                        "formSubType": {
                                            "type": "string"
                                        },
                                        "assignmentLocation": {
                                            "type": "object",
                                            "properties": {}
                                        }
                                    }
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "FileDB__files_upload_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/files"
                },
                "method": {
                    "type": "string",
                    "default": "post"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "data": {
                                    "type": "data"
                                }
                            }
                        },
                        "query": {
                            "type": "object",
                            "properties": {}
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{FileDB_settings.database_id}"
                                },
                                "X-Appery-Master-Key": {
                                    "type": "string",
                                    "default": "{FileDB_settings.master_key}"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "object",
                                    "properties": {}
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_WORKORDER_query_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/WORKORDER"
                },
                "method": {
                    "type": "string",
                    "default": "get"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {}
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "where": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                },
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "array",
                                    "items": [{
                                        "type": "object",
                                        "properties": {
                                            "assignmentLocation": {
                                                "type": "object",
                                                "properties": {}
                                            },
                                            "initDate": {
                                                "type": "string"
                                            },
                                            "updatedBy": {
                                                "type": "string"
                                            },
                                            "requestDate": {
                                                "type": "string"
                                            },
                                            "_id": {
                                                "type": "string"
                                            },
                                            "creatorId": {
                                                "type": "string"
                                            },
                                            "serviceAntecedent": {
                                                "type": "object",
                                                "properties": {}
                                            },
                                            "documentStatus": {
                                                "type": "number",
                                                "default": null
                                            },
                                            "initOperatorId": {
                                                "type": "string"
                                            },
                                            "assignmentDate": {
                                                "type": "string"
                                            },
                                            "rangeAttention": {
                                                "type": "number",
                                                "default": null
                                            },
                                            "deviceData": {
                                                "type": "object",
                                                "properties": {}
                                            },
                                            "endLocation": {
                                                "type": "object",
                                                "properties": {}
                                            },
                                            "documentCode": {
                                                "type": "string"
                                            },
                                            "acl": {
                                                "type": "object",
                                                "properties": {
                                                    "*": {
                                                        "type": "object",
                                                        "properties": {
                                                            "read": {
                                                                "type": "boolean",
                                                                "default": true
                                                            },
                                                            "write": {
                                                                "type": "boolean",
                                                                "default": true
                                                            }
                                                        }
                                                    }
                                                }
                                            },
                                            "formType": {
                                                "type": "string"
                                            },
                                            "operatorCode": {
                                                "type": "string"
                                            },
                                            "preferredDate": {
                                                "type": "string"
                                            },
                                            "maximumDate": {
                                                "type": "string"
                                            },
                                            "initLocation": {
                                                "type": "object",
                                                "properties": {}
                                            },
                                            "_updatedAt": {
                                                "type": "string"
                                            },
                                            "operationData": {
                                                "type": "object",
                                                "properties": {}
                                            },
                                            "_createdAt": {
                                                "type": "string"
                                            },
                                            "endDate": {
                                                "type": "string"
                                            },
                                            "previousDiagnosis": {
                                                "type": "object",
                                                "properties": {}
                                            },
                                            "formSubType": {
                                                "type": "string"
                                            },
                                            "receiverAntecedent": {
                                                "type": "object",
                                                "properties": {}
                                            },
                                            "receiverContact": {
                                                "type": "object",
                                                "properties": {}
                                            },
                                            "endOperatorId": {
                                                "type": "string"
                                            },
                                            "companyId": {
                                                "type": "string"
                                            },
                                            "productAntecedent": {
                                                "type": "object",
                                                "properties": {}
                                            },
                                            "createdBy": {
                                                "type": "string"
                                            }
                                        }
                                    }]
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_WORKORDER_delete_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/WORKORDER/{_id}"
                },
                "method": {
                    "type": "string",
                    "default": "delete"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {}
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "_id": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                },
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "object",
                                    "properties": {}
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "checkActivityAccount_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/code/ef3e3df1-63b3-4170-befd-cf41ab9cda8c/exec"
                },
                "method": {
                    "type": "string",
                    "default": "post"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "data": {
                                    "type": "data"
                                }
                            }
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "deleteSession": {
                                    "type": "string"
                                },
                                "lastSessionHour": {
                                    "type": "string"
                                },
                                "secEventBusyDetect": {
                                    "type": "string"
                                },
                                "_id": {
                                    "type": "string"
                                },
                                "lastSessionToken": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "Content-Type": {
                                    "type": "string",
                                    "default": "text/plain"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "object",
                                    "properties": {
                                        "requestBody": {
                                            "type": "string"
                                        },
                                        "requestParams": {
                                            "type": "string"
                                        }
                                    }
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "passwordRecovery_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/code/959a659f-016d-47cf-bf36-ce115562fb91/exec"
                },
                "method": {
                    "type": "string",
                    "default": "post"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "data": {
                                    "type": "data"
                                }
                            }
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "email": {
                                    "type": "string",
                                    "default": "cgfbsdfgsfg"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "Content-Type": {
                                    "type": "string",
                                    "default": "text/plain"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "object",
                                    "properties": {
                                        "requestParams": {
                                            "type": "string"
                                        },
                                        "requestBody": {
                                            "type": "string"
                                        }
                                    }
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_user_query_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/users"
                },
                "method": {
                    "type": "string",
                    "default": "get"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {}
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "where": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Master-Key": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id_su}"
                                },
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                },
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "array",
                                    "items": [{
                                        "type": "object",
                                        "properties": {
                                            "acl": {
                                                "type": "object",
                                                "properties": {
                                                    "*": {
                                                        "type": "object",
                                                        "properties": {
                                                            "write": {
                                                                "type": "boolean",
                                                                "default": true
                                                            },
                                                            "read": {
                                                                "type": "boolean",
                                                                "default": true
                                                            }
                                                        }
                                                    }
                                                }
                                            },
                                            "_id": {
                                                "type": "string"
                                            },
                                            "foto": {
                                                "type": "string"
                                            },
                                            "_createdAt": {
                                                "type": "string"
                                            },
                                            "username": {
                                                "type": "string"
                                            },
                                            "fullName": {
                                                "type": "string"
                                            },
                                            "_updatedAt": {
                                                "type": "string"
                                            },
                                            "codigoOperador": {
                                                "type": "string"
                                            },
                                            "email": {
                                                "type": "string"
                                            }
                                        }
                                    }]
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_OPERATORS_query_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/OPERATORS"
                },
                "method": {
                    "type": "string",
                    "default": "get"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {}
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "where": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                },
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "array",
                                    "items": [{
                                        "type": "object",
                                        "properties": {
                                            "profile": {
                                                "type": "string"
                                            },
                                            "lastReportGps": {
                                                "type": "string"
                                            },
                                            "_updatedAt": {
                                                "type": "string"
                                            },
                                            "_id": {
                                                "type": "string"
                                            },
                                            "idOperador": {
                                                "type": "string"
                                            },
                                            "idCliente": {
                                                "type": "string"
                                            },
                                            "estadoOperador": {
                                                "type": "boolean",
                                                "default": null
                                            },
                                            "_createdAt": {
                                                "type": "string"
                                            },
                                            "acl": {
                                                "type": "object",
                                                "properties": {
                                                    "*": {
                                                        "type": "object",
                                                        "properties": {
                                                            "read": {
                                                                "type": "boolean",
                                                                "default": true
                                                            },
                                                            "write": {
                                                                "type": "boolean",
                                                                "default": true
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }]
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_CLIENT_query_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/CLIENT"
                },
                "method": {
                    "type": "string",
                    "default": "get"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {}
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "where": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                },
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "array",
                                    "items": [{
                                        "type": "object",
                                        "properties": {
                                            "geoPos": {
                                                "type": "string"
                                            },
                                            "_createdAt": {
                                                "type": "string"
                                            },
                                            "dataAdd": {
                                                "type": "object",
                                                "properties": {}
                                            },
                                            "telefonoCliente": {
                                                "type": "string"
                                            },
                                            "_id": {
                                                "type": "string"
                                            },
                                            "idCliente": {
                                                "type": "string"
                                            },
                                            "nombreCliente": {
                                                "type": "string"
                                            },
                                            "clienteLogo": {
                                                "type": "string"
                                            },
                                            "_updatedAt": {
                                                "type": "string"
                                            },
                                            "emailNotificaciones": {
                                                "type": "string"
                                            },
                                            "dirCliente": {
                                                "type": "string"
                                            },
                                            "acl": {
                                                "type": "object",
                                                "properties": {
                                                    "*": {
                                                        "type": "object",
                                                        "properties": {
                                                            "write": {
                                                                "type": "boolean",
                                                                "default": true
                                                            },
                                                            "read": {
                                                                "type": "boolean",
                                                                "default": true
                                                            }
                                                        }
                                                    }
                                                }
                                            },
                                            "emailCliente": {
                                                "type": "string"
                                            }
                                        }
                                    }]
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_ORDERS_list_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/ORDERS"
                },
                "method": {
                    "type": "string",
                    "default": "get"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {}
                        },
                        "query": {
                            "type": "object",
                            "properties": {}
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                },
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "array",
                                    "items": [{
                                        "type": "object",
                                        "properties": {
                                            "_createdAt": {
                                                "type": "string"
                                            },
                                            "fechaInicioServicio": {
                                                "type": "string"
                                            },
                                            "tipoServicio": {
                                                "type": "string"
                                            },
                                            "acl": {
                                                "type": "object",
                                                "properties": {
                                                    "*": {
                                                        "type": "object",
                                                        "properties": {
                                                            "read": {
                                                                "type": "boolean",
                                                                "default": true
                                                            },
                                                            "write": {
                                                                "type": "boolean",
                                                                "default": true
                                                            }
                                                        }
                                                    }
                                                }
                                            },
                                            "fotos": {
                                                "type": "string"
                                            },
                                            "telefonoCliente": {
                                                "type": "string"
                                            },
                                            "mensajeCliente": {
                                                "type": "string"
                                            },
                                            "posGpsFinalizar": {
                                                "type": "string"
                                            },
                                            "codigo": {
                                                "type": "string"
                                            },
                                            "checkList": {
                                                "type": "string"
                                            },
                                            "idEmpresa": {
                                                "type": "string"
                                            },
                                            "descripcionReparacion": {
                                                "type": "string"
                                            },
                                            "_updatedAt": {
                                                "type": "string"
                                            },
                                            "comentarioCierre": {
                                                "type": "string"
                                            },
                                            "diagEspecialistaAudio": {
                                                "type": "string"
                                            },
                                            "correoCliente": {
                                                "type": "string"
                                            },
                                            "marca": {
                                                "type": "string"
                                            },
                                            "trabEspecialistaTexto": {
                                                "type": "string"
                                            },
                                            "investigacion": {
                                                "type": "string"
                                            },
                                            "direccion": {
                                                "type": "string"
                                            },
                                            "enDispositivo": {
                                                "type": "boolean",
                                                "default": null
                                            },
                                            "repuestos": {
                                                "type": "string"
                                            },
                                            "estado": {
                                                "type": "number",
                                                "default": null
                                            },
                                            "comuna": {
                                                "type": "string"
                                            },
                                            "fechaSolicitud": {
                                                "type": "string"
                                            },
                                            "fechaAsignacion": {
                                                "type": "string"
                                            },
                                            "trabEspecialistaAudio": {
                                                "type": "string"
                                            },
                                            "articulo": {
                                                "type": "string"
                                            },
                                            "fechaEstimada": {
                                                "type": "string"
                                            },
                                            "_id": {
                                                "type": "string"
                                            },
                                            "idOperador": {
                                                "type": "string"
                                            },
                                            "subTipoServicio": {
                                                "type": "string"
                                            },
                                            "fechaEjecucion": {
                                                "type": "string"
                                            },
                                            "nombreCliente": {
                                                "type": "string"
                                            },
                                            "diagEspecialistaTexto": {
                                                "type": "string"
                                            },
                                            "tipoCancelacion": {
                                                "type": "string"
                                            },
                                            "fechaCompra": {
                                                "type": "string"
                                            },
                                            "posGpsFinServicio": {
                                                "type": "string"
                                            },
                                            "descripcionDefecto": {
                                                "type": "string"
                                            }
                                        }
                                    }]
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_ORDERS_query_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/ORDERS"
                },
                "method": {
                    "type": "string",
                    "default": "get"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {}
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "where": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                },
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "array",
                                    "items": [{
                                        "type": "object",
                                        "properties": {
                                            "repuestos": {
                                                "type": "string"
                                            },
                                            "fechaEjecucion": {
                                                "type": "string"
                                            },
                                            "mensajeCliente": {
                                                "type": "string"
                                            },
                                            "articulo": {
                                                "type": "string"
                                            },
                                            "_createdAt": {
                                                "type": "string"
                                            },
                                            "fechaSolicitud": {
                                                "type": "string"
                                            },
                                            "telefonoCliente": {
                                                "type": "string"
                                            },
                                            "descripcionDefecto": {
                                                "type": "string"
                                            },
                                            "comuna": {
                                                "type": "string"
                                            },
                                            "diagEspecialistaTexto": {
                                                "type": "string"
                                            },
                                            "posGpsFinServicio": {
                                                "type": "string"
                                            },
                                            "idEmpresa": {
                                                "type": "string"
                                            },
                                            "tipoServicio": {
                                                "type": "string"
                                            },
                                            "fechaAsignacion": {
                                                "type": "string"
                                            },
                                            "trabEspecialistaAudio": {
                                                "type": "string"
                                            },
                                            "trabEspecialistaTexto": {
                                                "type": "string"
                                            },
                                            "posGpsFinalizar": {
                                                "type": "string"
                                            },
                                            "estado": {
                                                "type": "number",
                                                "default": null
                                            },
                                            "_id": {
                                                "type": "string"
                                            },
                                            "descripcionReparacion": {
                                                "type": "string"
                                            },
                                            "idOperador": {
                                                "type": "string"
                                            },
                                            "comentarioCierre": {
                                                "type": "string"
                                            },
                                            "fechaEstimada": {
                                                "type": "string"
                                            },
                                            "fechaInicioServicio": {
                                                "type": "string"
                                            },
                                            "tipoCancelacion": {
                                                "type": "string"
                                            },
                                            "fotos": {
                                                "type": "string"
                                            },
                                            "_updatedAt": {
                                                "type": "string"
                                            },
                                            "codigo": {
                                                "type": "string"
                                            },
                                            "nombreCliente": {
                                                "type": "string"
                                            },
                                            "direccion": {
                                                "type": "string"
                                            },
                                            "enDispositivo": {
                                                "type": "boolean",
                                                "default": null
                                            },
                                            "marca": {
                                                "type": "string"
                                            },
                                            "checkList": {
                                                "type": "string"
                                            },
                                            "fechaCompra": {
                                                "type": "string"
                                            },
                                            "investigacion": {
                                                "type": "string"
                                            },
                                            "diagEspecialistaAudio": {
                                                "type": "string"
                                            },
                                            "acl": {
                                                "type": "object",
                                                "properties": {
                                                    "*": {
                                                        "type": "object",
                                                        "properties": {
                                                            "read": {
                                                                "type": "boolean",
                                                                "default": true
                                                            },
                                                            "write": {
                                                                "type": "boolean",
                                                                "default": true
                                                            }
                                                        }
                                                    }
                                                }
                                            },
                                            "correoCliente": {
                                                "type": "string"
                                            },
                                            "subTipoServicio": {
                                                "type": "string"
                                            }
                                        }
                                    }]
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_ORDERS_read_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/ORDERS/{_id}"
                },
                "method": {
                    "type": "string",
                    "default": "get"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {}
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "_id": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                },
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "$": {
                                    "type": "object",
                                    "properties": {
                                        "tipoCancelacion": {
                                            "type": "string"
                                        },
                                        "enDispositivo": {
                                            "type": "boolean",
                                            "default": null
                                        },
                                        "posGpsFinServicio": {
                                            "type": "string"
                                        },
                                        "telefonoCliente": {
                                            "type": "string"
                                        },
                                        "trabEspecialistaAudio": {
                                            "type": "string"
                                        },
                                        "codigo": {
                                            "type": "string"
                                        },
                                        "direccion": {
                                            "type": "string"
                                        },
                                        "subTipoServicio": {
                                            "type": "string"
                                        },
                                        "correoCliente": {
                                            "type": "string"
                                        },
                                        "fechaEjecucion": {
                                            "type": "string"
                                        },
                                        "mensajeCliente": {
                                            "type": "string"
                                        },
                                        "fechaInicioServicio": {
                                            "type": "string"
                                        },
                                        "repuestos": {
                                            "type": "string"
                                        },
                                        "fechaCompra": {
                                            "type": "string"
                                        },
                                        "marca": {
                                            "type": "string"
                                        },
                                        "articulo": {
                                            "type": "string"
                                        },
                                        "fechaEstimada": {
                                            "type": "string"
                                        },
                                        "_updatedAt": {
                                            "type": "string"
                                        },
                                        "descripcionDefecto": {
                                            "type": "string"
                                        },
                                        "posGpsFinalizar": {
                                            "type": "string"
                                        },
                                        "diagEspecialistaTexto": {
                                            "type": "string"
                                        },
                                        "descripcionReparacion": {
                                            "type": "string"
                                        },
                                        "idEmpresa": {
                                            "type": "string"
                                        },
                                        "nombreCliente": {
                                            "type": "string"
                                        },
                                        "comuna": {
                                            "type": "string"
                                        },
                                        "estado": {
                                            "type": "number",
                                            "default": null
                                        },
                                        "fotos": {
                                            "type": "string"
                                        },
                                        "_id": {
                                            "type": "string"
                                        },
                                        "acl": {
                                            "type": "object",
                                            "properties": {
                                                "*": {
                                                    "type": "object",
                                                    "properties": {
                                                        "read": {
                                                            "type": "boolean",
                                                            "default": true
                                                        },
                                                        "write": {
                                                            "type": "boolean",
                                                            "default": true
                                                        }
                                                    }
                                                }
                                            }
                                        },
                                        "trabEspecialistaTexto": {
                                            "type": "string"
                                        },
                                        "tipoServicio": {
                                            "type": "string"
                                        },
                                        "_createdAt": {
                                            "type": "string"
                                        },
                                        "comentarioCierre": {
                                            "type": "string"
                                        },
                                        "idOperador": {
                                            "type": "string"
                                        },
                                        "checkList": {
                                            "type": "string"
                                        },
                                        "diagEspecialistaAudio": {
                                            "type": "string"
                                        },
                                        "fechaSolicitud": {
                                            "type": "string"
                                        },
                                        "investigacion": {
                                            "type": "string"
                                        },
                                        "fechaAsignacion": {
                                            "type": "string"
                                        }
                                    }
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        },
        "Tech1_ORDERS_update_service": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://api.appery.io/rest/1/db/collections/ORDERS/{_id}"
                },
                "method": {
                    "type": "string",
                    "default": "put"
                },
                "request": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "direccion": {
                                    "type": "string"
                                },
                                "articulo": {
                                    "type": "string"
                                },
                                "fechaCompra": {
                                    "type": "string"
                                },
                                "diagEspecialistaTexto": {
                                    "type": "string"
                                },
                                "descripcionReparacion": {
                                    "type": "string"
                                },
                                "diagEspecialistaAudio": {
                                    "type": "string"
                                },
                                "idEmpresa": {
                                    "type": "string"
                                },
                                "telefonoCliente": {
                                    "type": "string"
                                },
                                "fechaEstimada": {
                                    "type": "string"
                                },
                                "acl": {
                                    "type": "object",
                                    "properties": {
                                        "*": {
                                            "type": "object",
                                            "properties": {
                                                "read": {
                                                    "type": "boolean",
                                                    "default": true
                                                },
                                                "write": {
                                                    "type": "boolean",
                                                    "default": true
                                                }
                                            }
                                        }
                                    }
                                },
                                "fechaAsignacion": {
                                    "type": "string"
                                },
                                "checkList": {
                                    "type": "string"
                                },
                                "trabEspecialistaAudio": {
                                    "type": "string"
                                },
                                "comuna": {
                                    "type": "string"
                                },
                                "subTipoServicio": {
                                    "type": "string"
                                },
                                "tipoCancelacion": {
                                    "type": "string"
                                },
                                "investigacion": {
                                    "type": "string"
                                },
                                "estado": {
                                    "type": "number",
                                    "default": null
                                },
                                "descripcionDefecto": {
                                    "type": "string"
                                },
                                "codigo": {
                                    "type": "string"
                                },
                                "idOperador": {
                                    "type": "string"
                                },
                                "tipoServicio": {
                                    "type": "string"
                                },
                                "fotos": {
                                    "type": "string"
                                },
                                "mensajeCliente": {
                                    "type": "string"
                                },
                                "posGpsFinalizar": {
                                    "type": "string"
                                },
                                "fechaSolicitud": {
                                    "type": "string"
                                },
                                "correoCliente": {
                                    "type": "string"
                                },
                                "repuestos": {
                                    "type": "string"
                                },
                                "marca": {
                                    "type": "string"
                                },
                                "posGpsFinServicio": {
                                    "type": "string"
                                },
                                "trabEspecialistaTexto": {
                                    "type": "string"
                                },
                                "fechaEjecucion": {
                                    "type": "string"
                                },
                                "fechaInicioServicio": {
                                    "type": "string"
                                },
                                "nombreCliente": {
                                    "type": "string"
                                },
                                "enDispositivo": {
                                    "type": "boolean",
                                    "default": null
                                },
                                "comentarioCierre": {
                                    "type": "string"
                                }
                            }
                        },
                        "query": {
                            "type": "object",
                            "properties": {
                                "_id": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {
                                "X-Appery-Database-Id": {
                                    "type": "string",
                                    "default": "{Tech1_settings.database_id}"
                                },
                                "X-Appery-Session-Token": {
                                    "type": "string"
                                },
                                "Content-Type": {
                                    "type": "string",
                                    "default": "application/json"
                                }
                            }
                        }
                    }
                },
                "response": {
                    "type": "object",
                    "properties": {
                        "body": {
                            "type": "object",
                            "properties": {
                                "_updatedAt": {
                                    "type": "string"
                                }
                            }
                        },
                        "headers": {
                            "type": "object",
                            "properties": {}
                        }
                    }
                }
            }
        }
    };
    return models;
});