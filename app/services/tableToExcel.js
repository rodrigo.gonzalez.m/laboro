var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,';
  var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta charset = "utf-8"></head><body><table>{table}</table></body></html>';
  var base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) };
  var format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) };

  return function(table, name) {
    if (!table.nodeType) table = document.getElementById(table);
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML};
    var a = document.createElement('a');
    a.href = uri + base64(format(template, ctx));

    var dt = new Date();
    var day = dt.getDate();
    var month = dt.getMonth() + 1;
    var year = dt.getFullYear();
    var hour = dt.getHours();
    var mins = dt.getMinutes();
    var secs = dt.getSeconds();
    var postfix = year + "" + addZero(month) + "" + addZero(day) + "_" + addZero(hour) + "" + addZero(mins) + "" + addZero(secs);


    a.setAttribute('download', "lista_ordenes_" + postfix + ".xls");
    a.style.display = 'none';
    a.click();
    //window.location = a;
    //window.location.href = uri + base64(format(template, ctx));
  }
})
();

function addZero(n) {
    return n<10?'0'+n:''+n;
}