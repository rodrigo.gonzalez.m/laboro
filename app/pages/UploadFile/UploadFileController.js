define(['require', 'app'],
    function(require, APP) {
        /**
         * Controller for UploadFile generated by Appery.io
         * @module UploadFile
         */
        APP.controller('UploadFile', ['$scope', 'Apperyio', controller]);

        function controller($scope, Apperyio) {
            /**
             * user controller variables
             */
            $scope.files = Apperyio.EntityAPI('FIles');
            /**
             * User controller functions
             */
            /**
             * @function init
             */
            $scope.init = function() {
                document.getElementById('file_to_upload').addEventListener('change', function(e) {
                    $scope.files = [];
                    var el = document.getElementById('file_to_upload');
                    _.each(el.files, function(v) {
                        $scope.files.push({
                            filename: v.name,
                            isUploaded: false,
                            filepointer: v
                        });
                    });
                    $scope.$apply();
                });
            };
            /**
             * @function uploadAll
             */
            $scope.uploadAll = function() {
                if ($scope.files.length === 0) {
                    alert('No file for upload');
                    return;
                }
                var requestData = {};
                requestData = (function mapping9272($scope) {
                    var requestData = Apperyio.EntityAPI('FileDB__files_upload_service.request');
                    var user_scope = $scope.user;
                    var files_scope = $scope.files;
                    var formData = new FormData();
                    requestData.data = _.pluck($scope.files, 'filepointer');
                    return requestData;
                    /*|button_mapping|onbeforesend|DD8D892C-9F95-4E06-A792-A26575611C7D||9272|*/
                })($scope);
                Apperyio.get("FileDB__files_upload_service")(requestData).then( /*|service_bookmark|bookmark|DD8D892C-9F95-4E06-A792-A26575611C7D||7668|*/
                    function(success) {
                        alert('All files has been successfully uploaded.');
                        /*|button_mapping|onsuccess|DD8D892C-9F95-4E06-A792-A26575611C7D||5577|*/
                    },
                    function(error) {
                    },
                    function(notify) {
                    });
            };
        }
    });